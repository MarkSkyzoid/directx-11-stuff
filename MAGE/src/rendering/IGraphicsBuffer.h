#ifndef IGraphicsBuffer_h__
#define IGraphicsBuffer_h__

namespace mage
{

	// PURPOSE: Interface to wrap common graphics buffer functionalities.
	enum EGraphicsBufferType
	{
		VertexBuffer = 0,
		IndexBuffer,
		ConstantBuffer
	};

	class IGraphicsBuffer
	{
	public:
		IGraphicsBuffer(EGraphicsBufferType type) : m_type(type) {}

		virtual ~IGraphicsBuffer() {}

		virtual EMageResult Update(void* pData, u32 size, bool isDynamic = false) = 0;
		virtual EMageResult Update(void* pData, u32 stride, u32 count, bool isDynamic = false) = 0;
	
		virtual EMageResult Destroy() = 0;
	
		EGraphicsBufferType GetType() const { return m_type; }

		virtual u32			GetStride() const = 0;
		virtual u32			GetCount() const = 0; // Returns the number of elements in the buffer.

	protected:
		EGraphicsBufferType m_type;

		static const u16 k_numGraphicsBuffers = 2000;
	};
}
#endif // IGraphicsBuffer_h__
