#ifndef RenderDevice_h__
#define RenderDevice_h__

// Definitions
#ifdef MAGE_PLATFORM_WINDOWS
#include "D3D11/RenderDeviceD3D11.h"
typedef mage::RenderDeviceD3D11 RenderDeviceImpl;
#else
#pragma error("UNIMPLEMENTED RENDER DEVICE")
#endif //MAGE_PLATFORM_WINDOWS

#include "IGraphicsBuffer.h"

//PURPOSE: Base RenderDevice class. Wraps API calls.

//TODO: Create (parsable) configuration structure to initialize various systems (useful for buffer format, multisampling settings etc).
namespace mage
{
	class RenderDevice
	{
	public:
		RenderDevice() { /* ... */ }
		~RenderDevice() { /* ... */ }

		// Core functions.
		EMageResult Init(WindowHandle hWnd, u16 width, u16 height, bool fullscreen = false);
		EMageResult Shutdown();

		// Resource handling functions.
		IGraphicsBuffer* CreateVertexBuffer();
		IGraphicsBuffer* CreateIndexBuffer();
		IGraphicsBuffer* CreateConstantBuffer();

	private:
		RenderDeviceImpl m_implementation;
	};
}

#endif // RenderDevice_h__
