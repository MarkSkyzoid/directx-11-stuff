#include "../../pch.h"

// Mage includes

#include "RenderDeviceD3D11.h"

#include "GraphicsBufferD3D11.h"

#include "Texture2DD3D11.h"

#ifdef MAGE_PLATFORM_WINDOWS

namespace mage
{

	EMageResult RenderDeviceD3D11::Init(WindowHandle hWnd, u16 width, u16 height, bool fullscreen /* = false */)
	{
		m_pDevice = NULL;
		m_pDdeviceContext = NULL;
		m_pSwapChain = NULL;

		m_pBackBufferRTView = NULL;

		DXGI_SWAP_CHAIN_DESC swapChainDesc;
		ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

		swapChainDesc.BufferCount = 1;

		swapChainDesc.BufferDesc.Width = static_cast<UINT>(width);
		swapChainDesc.BufferDesc.Height = static_cast<UINT>(height);

		swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

		swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

		swapChainDesc.OutputWindow = static_cast<HWND>(hWnd);

		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;

		swapChainDesc.Windowed = !fullscreen;

		const D3D_FEATURE_LEVEL featureLevels[] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_10_0};
		const size_t featureLevelsCount = sizeof(featureLevels) / sizeof(featureLevels[0]);

		HRESULT hResult = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, 
			featureLevels, static_cast<UINT>(featureLevelsCount), D3D11_SDK_VERSION, 
			&swapChainDesc, &m_pSwapChain, &m_pDevice, NULL, &m_pDdeviceContext);

		if(FAILED(hResult))
			return MAGE_INIT_D3D11_DEVICE_FAIL;

		// Obtain and bind the back buffer render target to the pipeline.
		ID3D11Texture2D* pBackBufferTexture = NULL;
		hResult = m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBufferTexture);

		if(FAILED(hResult))
			return MAGE_INIT_D3D11_DEVICE_FAIL;

		m_pDevice->CreateRenderTargetView(pBackBufferTexture, NULL, &m_pBackBufferRTView);
		m_pDdeviceContext->OMSetRenderTargets(1, &m_pBackBufferRTView, NULL);

		//Viewport
		D3D11_VIEWPORT viewport;
		viewport.Width = width;
		viewport.Height = height;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		viewport.TopLeftX = 0.0f;
		viewport.TopLeftY = 0.0f; 

		m_pDdeviceContext->RSSetViewports(1, &viewport);
		return MAGE_RESULT_OK;
	}

	EMageResult RenderDeviceD3D11::Shutdown()
	{
		// Remove the buffers.
		for(u32 i = 0; i < m_vertexBuffers.size(); ++i)
		{
			mage_delete m_vertexBuffers[i];
		}
		m_vertexBuffers.clear();

		for(u32 i = 0; i < m_indexBuffers.size(); ++i)
		{
			mage_delete m_indexBuffers[i];
		}
		m_indexBuffers.clear();

		for(u32 i = 0; i < m_constantBuffers.size(); ++i)
		{
			mage_delete m_constantBuffers[i];
		}
		m_constantBuffers.clear();

		// Shutdown D3D 11 systems.
		m_pSwapChain->Release();
		m_pDevice->Release();
		m_pDdeviceContext->Release();
		return MAGE_RESULT_OK;
	}

	// Resource creation functions.
	IGraphicsBuffer* RenderDeviceD3D11::CreateVertexBuffer()
	{
		GraphicsBufferD3D11* vb = mage_new GraphicsBufferD3D11(VertexBuffer, this);
		m_vertexBuffers.push_back(vb);
		return vb;
	}

	IGraphicsBuffer* RenderDeviceD3D11::CreateIndexBuffer()
	{
		GraphicsBufferD3D11* ib = mage_new GraphicsBufferD3D11(IndexBuffer, this);
		m_indexBuffers.push_back(ib);
		return ib;
	}

	IGraphicsBuffer* RenderDeviceD3D11::CreateConstantBuffer()
	{
		GraphicsBufferD3D11* cb = mage_new GraphicsBufferD3D11(ConstantBuffer, this);
		m_constantBuffers.push_back(cb);
		return cb;
	}
}
#endif //MAGE_PLATFORM_WINDOWS