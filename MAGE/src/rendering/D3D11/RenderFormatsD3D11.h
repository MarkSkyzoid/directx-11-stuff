#ifndef RenderFormatsD3D11_h__
#define RenderFormatsD3D11_h__

#ifdef MAGE_PLATFORM_WINDOWS

// PURPOSE: Map render formats to Direct3D 11

namespace mage
{
	extern DXGI_FORMAT MageToD3D11RenderFormat[FORMAT_COUNT];
}

#endif // MAGE_PLATFORM_WINDOWS

#endif // RenderFormatsD3D11_h__
