#ifndef Texture2DD3D11_h__
#define Texture2DD3D11_h__

#ifdef MAGE_PLATFORM_WINDOWS 

#include "..\ITexture2D.h"

#include "RenderFormatsD3D11.h"

namespace mage
{
	class Texture2DD3D11 : public ITexture2D
	{
		REGISTER_POOL_CLASS(Texture2DD3D11, k_numTextures2D, 0)
	public:
		Texture2DD3D11(ETextureFlags flags, RenderDeviceD3D11* device);
		~Texture2DD3D11();

		virtual EMageResult Update( u32 width, u32 height, void* pData, ERenderResourceFormat format );

		virtual u32 GetWidth() const { return m_width; }

		virtual u32 GetHeight() const { return m_height; }

		virtual ERenderResourceFormat GetFormat() const { return m_format; }

	private:
		ETextureFlags m_flags;

		u32 m_width;
		u32 m_height;

		ERenderResourceFormat m_format;

		RenderDeviceD3D11* m_device;
	};
}

#endif // MAGE_PLATFORM_WINDOWS

#endif // Texture2DD3D11_h__
