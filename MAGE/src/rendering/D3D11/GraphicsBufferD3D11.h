#ifndef GraphicsBufferD3D11_h__
#define GraphicsBufferD3D11_h__

#ifdef MAGE_PLATFORM_WINDOWS

#include "../IGraphicsBuffer.h"

namespace mage
{
	// Forward Declarations
	class RenderDeviceD3D11;

	class GraphicsBufferD3D11 : public IGraphicsBuffer
	{
		REGISTER_POOL_CLASS(mage::GraphicsBufferD3D11, k_numGraphicsBuffers, 0) // The buffers will be allocated in a specific pool. 

	public:
		GraphicsBufferD3D11(EGraphicsBufferType type, RenderDeviceD3D11* device);

		virtual ~GraphicsBufferD3D11();

		virtual EMageResult Update(void* pData, u32 size, bool isDynamic = false);
		virtual EMageResult Update(void* pData, u32 stride, u32 count, bool isDynamic = false);

		virtual EMageResult Destroy();

		virtual u32			GetStride() const { return m_stride; }
		virtual u32			GetCount() const {return m_count; } // Returns the number of elements in the buffer.
	
	private:
		GraphicsBufferD3D11();

	private:
		u32				m_stride;
		u32				m_count;
		bool			m_isDynamic;
		ID3D11Buffer*	m_pBuffer;

		RenderDeviceD3D11* m_pDevice;
	};
}

#endif // MAGE_PLATFORM_WINDOWS

#endif // GraphicsBufferD3D11_h__
