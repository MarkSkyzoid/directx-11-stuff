#include "../../pch.h"

#ifdef MAGE_PLATFORM_WINDOWS

#include "RenderDeviceD3D11.h"

#include "GraphicsBufferD3D11.h"

DEFINE_POOL_CLASS(mage::GraphicsBufferD3D11)

namespace mage
{
	static D3D11_BIND_FLAG g_MAGEToD3D11Mapping[] =
	{
		D3D11_BIND_VERTEX_BUFFER,
		D3D11_BIND_INDEX_BUFFER,
		D3D11_BIND_CONSTANT_BUFFER
	};

	GraphicsBufferD3D11::GraphicsBufferD3D11(EGraphicsBufferType type, RenderDeviceD3D11* device) :
		IGraphicsBuffer(type),
		m_stride(0),
		m_count(0),
		m_isDynamic(false),
		m_pBuffer(NULL),
		m_pDevice(device)
	{

	}

	GraphicsBufferD3D11::~GraphicsBufferD3D11()
	{
		Destroy();
	}


	EMageResult GraphicsBufferD3D11::Update( void* pData, u32 size, bool isDynamic /*= false */)
	{
		if( pData == NULL || size <= 0)
			return MAGE_INVALID_ARGUMENTS;

		if(m_pDevice == NULL)
			return MAGE_INVALID_RENDER_DEVICE;

		// A constant buffer is alway treated as dynamic.
		isDynamic = isDynamic || (m_type == ConstantBuffer);

		// TODO: Update if buffer already exists.
		if(m_pBuffer != NULL)
		{
			if(m_isDynamic != isDynamic)
			{
				// We can't update this buffer.
				return MAGE_INVALID_BUFFER_UPDATE;
			}

			// Map buffer.
			D3D11_MAPPED_SUBRESOURCE mappedSubresource;
			ZeroMemory(&mappedSubresource, sizeof(mappedSubresource));

			if( FAILED(m_pDevice->GetDeviceContext()->Map(m_pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource)) )
				return MAGE_MAP_BUFFER_FAILED;

			memcpy(mappedSubresource.pData, pData, size);

			m_pDevice->GetDeviceContext()->Unmap(m_pBuffer, 0);

			return MAGE_RESULT_OK;
		}

		m_isDynamic = isDynamic;

		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(desc));

		desc.ByteWidth = size;
		desc.Usage = m_isDynamic ? D3D11_USAGE_DYNAMIC : D3D11_USAGE_DEFAULT;
		desc.BindFlags = g_MAGEToD3D11Mapping[m_type];
		desc.CPUAccessFlags = m_isDynamic ? D3D11_CPU_ACCESS_WRITE : 0;
		desc.MiscFlags = 0;
		desc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA data;
		ZeroMemory(&data, sizeof(data));
		data.pSysMem = pData;
		data.SysMemPitch = 0;
		data.SysMemSlicePitch = 0;

		if( FAILED(m_pDevice->GetDevice()->CreateBuffer(&desc, &data, &(m_pBuffer))) )
		{
			Destroy();
			return MAGE_BUFFER_CREATION_FAILED;
		}

		return MAGE_RESULT_OK;
	}

	EMageResult GraphicsBufferD3D11::Update( void* pData, u32 stride, u32 count, bool isDynamic /*= false*/ )
	{
		if( pData == NULL || stride <= 0 || count <= 0 )
			return MAGE_INVALID_ARGUMENTS;

		m_stride = stride;
		m_count = count;

		return Update(pData, m_stride * m_count, isDynamic);
	}

	EMageResult GraphicsBufferD3D11::Destroy()
	{
		if(m_pBuffer != NULL) 
		{
			m_pBuffer->Release();
			m_pBuffer = NULL;
		}

		return MAGE_RESULT_OK;
	}
}

#endif // MAGE_PLATFORM_WINDOWS