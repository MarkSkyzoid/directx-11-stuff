#ifndef RenderDeviceD3D11_h__
#define RenderDeviceD3D11_h__

#ifdef  MAGE_PLATFORM_WINDOWS

// library includes.
#include <vector>

// PURPOSE: Implementation of DirectX 11 Render Device.
namespace mage
{
	// Forward Declarations.
	class IGraphicsBuffer;
	class GraphicsBufferD3D11;

	class ITexture2D;
	class Texture2DD3D11;

	// PURPOSE: Implementation of DirectX 11 Render Device.
	class RenderDeviceD3D11
	{
	public:
		RenderDeviceD3D11() { }
		~RenderDeviceD3D11() { }

		// Accessors.
		ID3D11DeviceContext* GetDeviceContext() { return m_pDdeviceContext; }
		ID3D11Device*		 GetDevice() { return m_pDevice; }

		// Core functions.
		EMageResult Init(WindowHandle hWnd, u16 width, u16 height, bool fullscreen = false);
		EMageResult Shutdown();

		// Resource creation functions.
		IGraphicsBuffer* CreateVertexBuffer();
		IGraphicsBuffer* CreateIndexBuffer();
		IGraphicsBuffer* CreateConstantBuffer();

	private:
		// DirectX 11 Device and Swap Chain
		ID3D11Device*			m_pDevice;
		ID3D11DeviceContext*	m_pDdeviceContext;
		IDXGISwapChain*			m_pSwapChain;

		// Back buffer render target view.
		// TODO: Wrap render targets for generic usage.
		ID3D11RenderTargetView* m_pBackBufferRTView;

		// Graphics Device Resources.
		// TODO: Consider removing :)
		std::vector<GraphicsBufferD3D11* > m_vertexBuffers;
		std::vector<GraphicsBufferD3D11* > m_indexBuffers;
		std::vector<GraphicsBufferD3D11* > m_constantBuffers;
	};
}

#endif //  MAGE_PLATFORM_WINDOWS

#endif // RenderDeviceD3D11_h__
