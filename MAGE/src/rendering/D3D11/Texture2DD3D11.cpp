#include "../../pch.h"

#ifdef MAGE_PLATFORM_WINDOWS
#include "RenderDeviceD3D11.h"

#include "Texture2DD3D11.h"

DEFINE_POOL_CLASS(mage::Texture2DD3D11)

namespace mage
{
	Texture2DD3D11::Texture2DD3D11(ETextureFlags flags, RenderDeviceD3D11* device) :
		ITexture2D(),
		m_flags(flags),
		m_width(0),
		m_height(0),
		m_format(FORMAT_UNKNOWN),
		m_device(device)
	{

	}

	Texture2DD3D11::~Texture2DD3D11()
	{

	}

	// Updates the contents of a texture.
	mage::EMageResult Texture2DD3D11::Update( u32 width, u32 height, void* pData, ERenderResourceFormat format )
	{
		// TODO: Write the GODDAMN DX CODE :D
		// Avevo sonno, davvero <3 GN :D
		return MAGE_RESULT_OK;
	}
}

#endif //MAGE_PLATFORM_WINDOWS