// mage includes
#include "../pch.h"
#include "RenderDevice.h"

namespace mage
{

	EMageResult RenderDevice::Init( WindowHandle hWnd, u16 width, u16 height, bool fullscreen /* = false */ )
	{
		return m_implementation.Init(hWnd, width, height, fullscreen);
	}

	EMageResult RenderDevice::Shutdown()
	{
		return m_implementation.Shutdown();
	}

	// Resource Creation functions.
	IGraphicsBuffer* RenderDevice::CreateVertexBuffer()
	{
		return m_implementation.CreateVertexBuffer();
	}

	IGraphicsBuffer* RenderDevice::CreateIndexBuffer()
	{
		return m_implementation.CreateIndexBuffer();
	}

	IGraphicsBuffer* RenderDevice::CreateConstantBuffer()
	{
		return m_implementation.CreateConstantBuffer();
	}
}