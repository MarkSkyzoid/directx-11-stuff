#ifndef RenderFormats_h__
#define RenderFormats_h__

// PURPOSE: Define common rendering formats such as texture formats and so on.
namespace mage
{
	// Common formats use by resources (mainly textures).
	// TODO: Add more as needed.
	// TODO: As they get added update corresponding representation in implementation specific files.
	enum ERenderResourceFormat
	{
		FORMAT_UNKNOWN = 0,
		
		FORMAT_RGBA32_FLOAT,
		FORMAT_RGBA32_UINT,

		FORMAT_RGB32_FLOAT,
		FORMAT_RGB32_UINT,

		FORMAT_D24_UNORM_S8_UINT,		// Depth/Stencil

		FORMAT_COUNT					// Max number of formats.
	};
}

#endif // RenderFormats_h__
