#ifndef ITexture2D_h__
#define ITexture2D_h__

#include "RenderFormats.h"

namespace mage
{
	// PURPOSE: Flags for texture creation.
	enum ETextureFlags
	{
		TextureFlagNone = 0,
		TextureFlagMipmapped = 1,				// Generate Mip Maps.
		TextureFlagRenderTarget = 1 << 2		// Bind as Render Target.
	};


	// PURPOSE: Interface to represent a 2D texture resource for  given graphics context.
	class ITexture2D
	{
	public:
		ITexture2D() {}
		virtual ~ITexture2D() {}

		virtual EMageResult Update(u32 width, u32 height, void* pData, ERenderResourceFormat format) = 0;

		virtual u32 GetWidth() const = 0;
		virtual u32 GetHeight() const = 0;

		virtual ERenderResourceFormat GetFormat() const = 0;

	protected:
		static const u16 k_numTextures2D = 2000;
	};
}

#endif // ITexture2D_h__
