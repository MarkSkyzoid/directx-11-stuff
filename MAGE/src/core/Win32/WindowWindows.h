#ifndef WindowWindows_h__
#define WindowWindows_h__

// mage includes
#include "../Window.h"

#ifdef MAGE_PLATFORM_WINDOWS

namespace mage
{
	class WindowWindows : public Window
	{
	public:

		WindowWindows(HINSTANCE hInstance, WNDPROC wndProc);

		virtual ~WindowWindows();

		virtual EMageResult Init( u16 width, u16 height, bool fullscreen = false );

		virtual EMageResult Shutdown();

		virtual EMageResult Update();

		virtual const WindowHandle& GetHandle() const
		{
			return m_hWnd;
		}

	private:
		WindowWindows();

	private:
		WindowHandle m_hWnd;
		HINSTANCE m_hInstance;
		WNDPROC m_wndProc;
	};
}

#endif // MAGE_PLATFORM_WINDOWS

#endif // WindowWindows_h__
