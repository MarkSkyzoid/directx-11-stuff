#include "../../pch.h"
#include "WindowWindows.h"

#ifdef MAGE_PLATFORM_WINDOWS


namespace mage
{

	WindowWindows::WindowWindows(HINSTANCE hInstance, WNDPROC wndProc) : mage::Window()
	{
		m_hInstance = hInstance;
		m_wndProc = wndProc;
	}

	WindowWindows::~WindowWindows()
	{

	}

	EMageResult mage::WindowWindows::Init( u16 width, u16 height, bool fullscreen /* = false */ )
	{
		WNDCLASSEX wcex;
		wcex.cbSize			= sizeof(WNDCLASSEX);
		wcex.style          = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc    = m_wndProc;
		wcex.cbClsExtra     = 0;
		wcex.cbWndExtra     = 0;
		wcex.hInstance      = m_hInstance;
		wcex.hIcon          = NULL;
		wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
		wcex.lpszMenuName   = NULL;
		wcex.lpszClassName  = (LPCTSTR)"MAGEWindowClass";
		wcex.hIconSm		= NULL;
		if( !RegisterClassEx(&wcex) )
		{
			MessageBoxEx(NULL, (LPCTSTR)"FAILED to Initialize the Window", (LPCTSTR)"Error!", MB_OK, 0);
			return MAGE_INIT_WIN32_WINDOW_FAIL;
		}

		RECT rc = { 0, 0, width, height };
		AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
		m_hWnd = CreateWindowEx( NULL, (LPCTSTR)"MAGEWindowClass", "MAGE Application", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, m_hInstance,
			NULL );

		if(m_hWnd == NULL)
		{
			MessageBoxEx(NULL, (LPCTSTR)"FAILED to Initialize the Window", (LPCTSTR)"Error!", MB_OK, 0);
			return MAGE_INIT_WIN32_WINDOW_FAIL;
		}

		ShowWindow(m_hWnd, SW_SHOW);
		UpdateWindow(m_hWnd);

		return MAGE_RESULT_OK;
	}

	EMageResult WindowWindows::Shutdown()
	{
		return MAGE_RESULT_OK;
	}

	EMageResult WindowWindows::Update()
	{
		MSG message;
		while (PeekMessage(&message,NULL,0,0,PM_REMOVE))
		{
			if (message.message == WM_QUIT)
				return MAGE_WINDOW_UPDATE_FAIL;

			TranslateMessage(&message);
			DispatchMessage(&message);
		}

		return MAGE_RESULT_OK;
	}

}

#endif //MAGE_PLATFORM_WINDOWS