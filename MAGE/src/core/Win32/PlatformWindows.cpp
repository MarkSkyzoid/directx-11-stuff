// mage includes
#include "../../pch.h"
#include "../Platform.h"
#include "WindowWindows.h"

#ifdef MAGE_PLATFORM_WINDOWS

// SDKs includes
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

struct WindowsAttributes
{
	HINSTANCE	hInstance;
};

WindowsAttributes& GetWindowsAttributesInstance()
{
	static WindowsAttributes gWindowsAttributes;
	return gWindowsAttributes;
}

#define WINDOWSATTRIBUTES GetWindowsAttributesInstance()

int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
	UNREFERENCED_PARAMETER(hInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO:
	// - Read Window Information from configuration file.
	// - Create Window based on this information.

	WINDOWSATTRIBUTES.hInstance = hInstance;

	s32 returnValue = mageMain();

	return returnValue;
}

LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch( message )
	{
	case WM_PAINT:
		hdc = BeginPaint( hWnd, &ps );
		EndPaint( hWnd, &ps );
		break;

	case WM_DESTROY:
		PostQuitMessage( 0 );
		break;

    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE)
        {
            PostQuitMessage(0);
        }
        break;
	default:
		break;
	}

	return DefWindowProc( hWnd, message, wParam, lParam );;
}

mage::Window* mage::platform::CreateSystemWindow()
{
	Window* window = new WindowWindows(WINDOWSATTRIBUTES.hInstance, WndProc);

	return window;
}

#endif // MAGE_PLATFORM_WINDOWS
