#ifndef Window_h__
#define Window_h__
// PURPOSE: Base class for windows.

namespace mage
{
	class Window
	{
	public:
		Window() {}
		virtual ~Window() {}

		virtual EMageResult Init(u16 width, u16 height, bool fullscreen = false) = 0;
		virtual EMageResult Shutdown() = 0;

		virtual EMageResult Update() = 0;

		virtual const WindowHandle& GetHandle() const = 0;
	};
}

#endif // Window_h__
