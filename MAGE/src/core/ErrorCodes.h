#ifndef ErrorCodes_h__
#define ErrorCodes_h__

namespace mage
{
	// PURPOSE: Enumeration containing all sorts of errors.
	// The first value (= 0) mean the operation was successful.

	enum EMageResult
	{
		MAGE_RESULT_OK = 0,					// Successful operation! 
		
		MAGE_INVALID_ARGUMENTS,				// Function has been called with invalid arguments.
		
		// Memory

		MAGE_OUT_OF_MEMORY,					// Allocation didn't succeed.

		// Rendering

		MAGE_INVALID_RENDER_DEVICE,			// Render device might not exist or be invalid. Should never happen.

		MAGE_BUFFER_CREATION_FAILED,		// Failed to create a graphics buffer.

		MAGE_INVALID_BUFFER_UPDATE,			// Trying to update a buffer that is static.

		MAGE_MAP_BUFFER_FAILED,				// Failed to map a graphics buffer.

		// Platform

#ifdef MAGE_PLATFORM_WINDOWS

		MAGE_INIT_WIN32_WINDOW_FAIL,		// Couldn't initialize Win32 Window.

		MAGE_INIT_D3D11_DEVICE_FAIL,		// Couldn't initialize DX11 Device.

		MAGE_WINDOW_UPDATE_FAIL,			// Couldn't update window (doesn't mean critical error).

#endif // MAGE_PLATFORM_WINDOWS

		MAGE_RESULTS_COUNT					// Number of error codes.
	};

#define MAGE_FAILURE(x) (x != 0) // 0 = MAGE_RESULT_OK
#define MAGE_SUCCESS(x) (x == 0)
}

#endif // ErrorCodes_h__
