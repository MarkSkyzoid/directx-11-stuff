#ifndef MageTypes_h__
#define MageTypes_h__

typedef uint8_t		u8;
typedef uint16_t	u16;
typedef uint32_t	u32;
typedef uint64_t	u64;

typedef int8_t		s8;
typedef int16_t		s16;
typedef int32_t		s32;
typedef	int64_t		s64;

#ifdef MAGE_PLATFORM_WINDOWS

#if _WIN32
typedef u32			uptr;
#elif _WIN64
typedef u64			uptr;
#endif // _WIN32

typedef HWND WindowHandle;
#else
typedef void* WindowHandle;
#endif // MAGE_PLATFORM_WINDOWS

#endif // MageTypes_h__
