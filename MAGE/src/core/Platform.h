#ifndef Platform_h__
#define Platform_h__

// PURPOSE: Declaration of MAGE user's entry point. 
// Define this as you would normally do with a main function.
int mageMain();

namespace mage
{
	// Forward declarations.
	class Window;

	namespace platform
	{
		mage::Window* CreateSystemWindow();
	}
}

#endif // Platform_h__
