#ifndef PlatformDetection_h__
#define PlatformDetection_h__

#ifdef _WIN32
#define MAGE_PLATFORM_WINDOWS
#else
#define MAGE_PLATFORM_OTHER
#pragma error("Unsupported Platform!")
#endif // _WIN32

#endif // PlatformDetection_h__
