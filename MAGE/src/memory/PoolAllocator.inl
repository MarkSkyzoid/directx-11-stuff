namespace mage
{
	template<typename T, u32 ObjectCount, size_t PoolAlignment>
	inline EMageResult PoolAllocator<T, ObjectCount, PoolAlignment>::Init()
	{
		if(ObjectCount <= 0)
			return MAGE_INVALID_ARGUMENTS;

		// Create the required memory.
		if(m_memory != NULL)
		{
			delete[] m_memory;
			m_memory = NULL;
		}

		const size_t objectSize = sizeof(T);

		const size_t totalSize = ObjectCount * objectSize + PoolAlignment;

		m_memory = new MemType[totalSize];

		if(m_memory == NULL)
		{
			return MAGE_OUT_OF_MEMORY;
		}

		// Align the memory and pass it to the free list.
		// We keep the original allocated memory (without alignment) to free it later.
		// It is IMPORTANT to use the original address returned by new when calling delete.
		void* pStart = AlignUp(m_memory, PoolAlignment);

		//EMageResult result = m_freeList.Init(pStart, ObjectCount, objectSize);
		if(m_stack != NULL)
		{
			delete[] m_stack;
			m_stack = NULL;
		}

		m_stack = new void*[ObjectCount];
		m_stackPtr = 0;

		if(m_stack == NULL)
		{
			return MAGE_OUT_OF_MEMORY;
		}

		// Init with pointers.
		for(u32 i = 0; i < ObjectCount; ++i)
		{
			m_stack[i] = pStart;
			pStart = (MemPtr)pStart + objectSize;
			++m_stackPtr;
		}

		return MAGE_RESULT_OK;
	}

	template<typename T, u32 ObjectCount, size_t PoolAlignment>
	inline EMageResult PoolAllocator<T, ObjectCount, PoolAlignment>::Shutdown()
	{
		if(m_memory != NULL)
		{
			delete[] m_memory;
			m_memory = NULL;
		}

		if(m_stack != NULL)
		{
			delete[] m_stack;
			m_stack = NULL;
		}

		m_stackPtr = 0;

		return MAGE_RESULT_OK;
	}

	template<typename T, u32 ObjectCount, size_t PoolAlignment>
	inline void* PoolAllocator<T, ObjectCount, PoolAlignment>::Allocate()
	{
		void* pData = NULL;
		if(m_stackPtr > 0)
		{
			pData = m_stack[m_stackPtr - 1];
			--m_stackPtr;
		}
		return pData;
	}

	template<typename T, u32 ObjectCount, size_t PoolAlignment>
	inline void PoolAllocator<T, ObjectCount, PoolAlignment>::Deallocate( void* pData )
	{
		m_stack[m_stackPtr] = pData;
		++m_stackPtr;
		if(m_stackPtr == ObjectCount)
		{
			m_stackPtr = 0;
		}
	}
}