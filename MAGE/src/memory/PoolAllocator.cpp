#include "../pch.h"

namespace mage
{
	void* AlignUp(void* const ptr, size_t alignment)
	{
		uptr address = (uptr) ptr;
		uptr mask = alignment ? alignment - 1 : 0;
		uptr misalignment = address & mask;
		uptr adjustment = alignment - misalignment;  
		return (void*)(address + adjustment * (misalignment != 0)); 
	}
}