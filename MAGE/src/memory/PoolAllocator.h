#ifndef PoolAllocator_h__
#define PoolAllocator_h__

#define mage_new new
#define mage_delete delete

#define REGISTER_POOL_CLASS(_CLASS, _COUNT, _ALIGNMENT) \
	private: \
	typedef mage::PoolAllocator<_CLASS, _COUNT, _ALIGNMENT> Pool; \
	static Pool m_gPool; \
	public: \
	void* operator new(size_t bytes) \
	{ \
		return m_gPool.Allocate(); \
	} void operator delete (void* ptr) { m_gPool.Deallocate(ptr); }

#define DEFINE_POOL_CLASS(_CLASS) _CLASS::Pool _CLASS::m_gPool;

namespace mage
{
	void* AlignUp(void* const ptr, size_t alignment);

	// PURPOSE: Default allocator of MAGE. Used by classes that require dynamic allocation.
	template<typename T, u32 ObjectCount, size_t PoolAlignment = 0>
	class PoolAllocator
	{
	private:
		typedef unsigned char MemType;
		typedef MemType* MemPtr;

		typedef void** FreeStack;

	public:
		PoolAllocator() : m_memory (NULL), m_stack(NULL), m_stackPtr(0) { Init();}
		~PoolAllocator() { Shutdown(); }

		EMageResult Shutdown();

		void* Allocate();

		void Deallocate( void* pData );

	private:
		EMageResult Init();

	public:
		MemPtr m_memory; // The memory where we store all the objects.

		FreeStack m_stack; 
		u32 m_stackPtr;
	};
}

#include "PoolAllocator.inl"

#endif // PoolAllocator_h__
