#ifndef _PCH_H
#define _PCH_H

//Platform Detection
#include "core/PlatformDetection.h"

// std includes.
#include <cstdint>

#ifdef MAGE_PLATFORM_WINDOWS

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>

//#include <d3d11.h>
#include <d3d11.h>
#include <d3d10.h>

#endif // MAGE_PLATFORM_WINDOWS

//Mage Includes
#include "core/MageTypes.h"
#include "core/ErrorCodes.h"
#include "memory/PoolAllocator.h"

#endif // _PCH_H
