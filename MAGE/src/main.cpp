#include "pch.h"
#include "core/Platform.h"
#include "core/Window.h"
#include "rendering/RenderDevice.h"

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 720

s32 mageMain()
{
	// Init.
	// TODO: Move Init to Engine class.

	mage::Window* window = mage::platform::CreateSystemWindow();
	if(window == NULL)
		return 1; //Error!

	if( MAGE_FAILURE(window->Init(WINDOW_WIDTH, WINDOW_HEIGHT)) )
		return 1; // Error!

	mage::RenderDevice renderDevice;
	if( MAGE_FAILURE(renderDevice.Init(window->GetHandle(), WINDOW_WIDTH, WINDOW_HEIGHT)) )
		return 1;

	// TODO: Make Unit tests :D
	// Test buffer creation functionalities.
	mage::IGraphicsBuffer* vertexBuffer = renderDevice.CreateVertexBuffer();
	if(vertexBuffer != NULL)
	{
		struct VertexStruct 
		{
			float x, y, z;
		};

		VertexStruct vertices[3];
		for(u32 i = 0; i < 3; ++i)
		{
			vertices[i].x = vertices[i].y = vertices[i].z = 0.0f;
		}

		vertexBuffer->Update(vertices, sizeof(VertexStruct), 3, true);
	}

	// TODO: All this code is part of the main loop and should go into the Engine class.
	while(true)
	{
		if( MAGE_FAILURE(window->Update()) )
			break;
	}

	// Shutdown.
	// TODO: Move shutdown to engine class.

	renderDevice.Shutdown();
	
	window->Shutdown();
	delete window; // TODO: Use proper resource management classes.

	return 0;
}


// Temporary fall back when running the project with SubSystem = Console rather than Windows.
int main(void)
{
    return mageMain();
}